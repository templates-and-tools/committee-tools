# FOO BAR Perl Committee
# Membership Roster

## Officers

* FOO BAR, Chairman, elected X/XX/XX
* FOO BAR, 1st Vice Chairman, elected X/XX/XX
* FOO BAR, 2nd Vice Chairman, elected X/XX/XX


## Current

* FOO BAR
* FOO BAR, Co-Founder
* FOO BAR


## Invited

* FOO BAR
* FOO BAR
* FOO BAR


## Need Invite

* FOO BAR
* FOO BAR
* FOO BAR


## Declined

* FOO BAR [ NOT INTERESTED IN FOO BAR ]
* FOO BAR [ CUSTOMER ONLY USING PYTHON ]
* FOO BAR [ NO FOO BAR FOR NOW ]
* FOO BAR [ NOT USING FOO BAR ]
