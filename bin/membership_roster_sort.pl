#!/usr/bin/env perl
# Copyright © 2023, William N. Braswell, Jr.. All Rights Reserved. This work is Free & Open Source; you can redistribute it and/or modify it under the same terms as Perl.

use strict;
use warnings;
our $VERSION = 0.001_000;

use Data::Dumper;
use Perl::Types;

my string::hashref $members = {};

# only sort one membership roster file at a time
my string $arg = $ARGV[0];
die if not -f $arg;

# read in membership file in Markdown format
#print "reading $arg... ";
(open my $FH, "<", $arg) or die $!;

my $s = q{};
while (<$FH>) { $s .= $_; }
#print "have \$s = $s\n\n";
(close $FH) or die $!;

# print an extra blank line above not-first-section headings
my boolean $is_first_section = 1;

# skip sections that should not be sorted
my boolean $skip_section = 0;

# current line number of input file
my integer $line_number = 0;

# process and sort sections
foreach my string $line (split "\n", $s) {
#print 'have $members = ', Dumper($members);
    $line_number++;

    # setion headings
    if ((substr $line, 0, 2) eq '##') {
        # print sorted list of members in previous section (if any), reset list of members
        print "\n";
        foreach my string $name_sorted (sort keys %{$members}) {
            print $members->{$name_sorted}, "\n";
        }
        $members = {};

        # don't sort presumably-already-sorted list of officers:
        # ## Officers
        if ($line eq '## Officers') {
            # set flag to skip entire section
            $skip_section = 1;
        }
        # do sort non-officer lists:
        # ## Current
        # ## Invited
        # etc
        else {
            # unset flag to skip entire section
            $skip_section = 0;
        }

        if (not $is_first_section) {
            # print extra blank line above all section headings except the first one
            print "\n";            
        }

        # print current section heading line, plus a blank line following the heading
        print "\n", $line, "\n";

        # you can only be the first section once!
        $is_first_section = 0;
    }
    # skipped section
    elsif ($skip_section) {
        # print current skipped section line
        if ($line ne '') {
            print "\n", $line;
        }
    }
    # aggregate individual member names;
    # * John Smith (FOO BAR) 
    elsif ($line =~ m/^\*\s+([\w\s]+)(.*)?$/) {
        my string::arrayref $names = [split /\s+/, $1];
        my string $name_sortable = (pop @{$names}) . ', ';
        $name_sortable .= join ' ', @{$names};
        $members->{$name_sortable} = $line;
    }
    else {
        # print current non-section-heading, non-member line
        if ($line ne '') {
            # don't print an extra blank line above the first line of the input file
            if ($line_number > 1) {
                print "\n";
            }
            print $line;
        }
    }
}

# print sorted list of members in final section (if any)
print "\n";
foreach my string $name_sorted (sort keys %{$members}) {
    print $members->{$name_sorted}, "\n";
}

