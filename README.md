# FOO BAR Perl Committee

We are the official democratically-elected organization for oversight of FOO BAR in Perl.

## Vision Statement

Our vision is a world where Perl is the dominant language in FOO BAR development.

## Mission Statement

Our mission is to develop and promote FOO BAR in Perl.
