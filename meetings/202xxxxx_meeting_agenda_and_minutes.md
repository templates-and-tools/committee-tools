# FOO BAR Perl Committee
# Meeting Agenda & Minutes

## FOO XXth, 202X
## FOO Official Meeting

* Opening, X:XXpm

* Attendance & Introductions
    * FOO BAR (FOO BAR)

* Announcements
    * Meet every X weeks on FOOday X:XX-X:XXpm Central time zone
    * FOO discussion starting X/X/XX at next meeting

* Previous Meeting's Minutes
    * X/XX/XX meeting minutes
    * Read by Acting Secretary FOO BAR
    * Moved by FOO BAR to be accepted as read, voted unanimously

* Old Business

    * FOO BAR
        * FOO BAR
        * FOO BAR

* New Business

    * FOO BAR
        * FOO BAR
        * FOO BAR

    * Officer Elections
        * Chairman, FOO BAR nominated by FOO BAR, no other nominations, elected unanimously
        * 1st Vice Chairman, FOO BAR nominated by FOO BAR, no other nominations, elected unanimously
        * 2nd Vice Chairman, FOO BAR nominated by FOO BAR, no other nominations, elected unanimously

* Closing, X:XXpm
